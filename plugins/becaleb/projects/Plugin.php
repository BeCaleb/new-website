<?php namespace Becaleb\Projects;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
        'Becaleb\Projects\Components\filterprojects' => 'filterprojects',
      ];
    }

    public function registerSettings()
    {
    }
}

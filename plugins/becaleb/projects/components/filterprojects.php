<?php namespace Becaleb\Projects\Components;

use Cms\Classes\ComponentBase;

use Input;

use Becaleb\Projects\Models\Projects;
use Becaleb\Projects\Models\Categories;

class filterprojects extends ComponentBase

{

  public function componentDetails(){
    return [

      'name' => 'Filter projects',
      'description' => 'Filter projects page'

    ];
  }

  public function onRun(){
    $this->projects = $this->filterprojects();
    $this->categories = Categories::all();
  }

  protected function filterprojects(){

      $category = Input::get('category');

      $query = Projects::all();

      if($category){

        $query = Projects::whereHas('categories', function($filter) use ($category){
          $filter->where('slug', '=', $category);


        })->get();

      }


      return $query;
  }

  public $projects;
  public $categories;

}

<?php namespace Becaleb\Projects\Models;

use Model;

/**
 * Model
 */
class Projects extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'becaleb_projects_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    
    public $belongsToMany = [
        'categories' =>[
                'Becaleb\Projects\Models\Categories',
                'table' => 'becaleb_projects_cat_link',
                'order' => 'category',
                
            ],
        'members' =>[
                'Becaleb\Projects\Models\Teaam',
                'table' => 'becaleb_projects_team_link',
                'order' => 'name',
                
            ]
    ];
    public $attachMany = [
    'images' => 'System\Models\File'
    ];
    public $attachOne = [
    'cover' => 'System\Models\File'
    ];
}

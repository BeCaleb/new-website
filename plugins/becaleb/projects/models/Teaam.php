<?php namespace Becaleb\Projects\Models;

use Model;

/**
 * Model
 */
class Teaam extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'becaleb_projects_team';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}

<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBecalebProjectsCatLink extends Migration
{
    public function up()
    {
        Schema::create('becaleb_projects_cat_link', function($table)
        {
            $table->engine = 'InnoDB';
            $table->smallInteger('project_id');
            $table->smallInteger('category_id');
            $table->primary(['project_id','category_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('becaleb_projects_cat_link');
    }
}

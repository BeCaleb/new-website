<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBecalebProjectsTeamLink extends Migration
{
    public function up()
    {
        Schema::create('becaleb_projects_team_link', function($table)
        {
            $table->engine = 'InnoDB';
            $table->smallInteger('projects_id');
            $table->smallInteger('team_id');
            $table->primary(['projects_id','team_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('becaleb_projects_team_link');
    }
}

<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjects extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->string('quote')->nullable();
            $table->string('client_name_surname')->nullable();
            $table->string('client_company')->nullable();
            $table->text('brief')->nullable();
            $table->text('process')->nullable();
            $table->text('results')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->dropColumn('title');
            $table->dropColumn('sub_title');
            $table->dropColumn('quote');
            $table->dropColumn('client_name_surname');
            $table->dropColumn('client_company');
            $table->dropColumn('brief');
            $table->dropColumn('process');
            $table->dropColumn('results');
        });
    }
}

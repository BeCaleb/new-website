<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjects2 extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->dropColumn('results');
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->text('results')->nullable();
        });
    }
}

<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjects3 extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->string('title', 255)->nullable(false)->change();
            $table->string('sub_title', 255)->nullable(false)->change();
            $table->string('client_company', 255)->nullable(false)->change();
            $table->text('brief')->nullable(false)->change();
            $table->text('process')->nullable(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->string('title', 255)->nullable()->change();
            $table->string('sub_title', 255)->nullable()->change();
            $table->string('client_company', 255)->nullable()->change();
            $table->text('brief')->nullable()->change();
            $table->text('process')->nullable()->change();
        });
    }
}

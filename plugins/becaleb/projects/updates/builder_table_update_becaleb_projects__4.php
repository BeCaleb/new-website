<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjects4 extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}

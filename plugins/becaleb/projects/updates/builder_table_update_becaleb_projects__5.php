<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjects5 extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->string('quote_icon')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_', function($table)
        {
            $table->dropColumn('quote_icon');
        });
    }
}

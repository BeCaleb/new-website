<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjectsCatLink extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_cat_link', function($table)
        {
            $table->dropPrimary(['project_id','category_id']);
            $table->renameColumn('project_id', 'projects_id');
            $table->primary(['projects_id','category_id']);
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_cat_link', function($table)
        {
            $table->dropPrimary(['projects_id','category_id']);
            $table->renameColumn('projects_id', 'project_id');
            $table->primary(['project_id','category_id']);
        });
    }
}

<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjectsCatLink2 extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_cat_link', function($table)
        {
            $table->dropPrimary(['projects_id','category_id']);
            $table->renameColumn('category_id', 'categories_id');
            $table->primary(['projects_id','categories_id']);
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_cat_link', function($table)
        {
            $table->dropPrimary(['projects_id','categories_id']);
            $table->renameColumn('categories_id', 'category_id');
            $table->primary(['projects_id','category_id']);
        });
    }
}

<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjectsCategories extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_categories', function($table)
        {
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_categories', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}

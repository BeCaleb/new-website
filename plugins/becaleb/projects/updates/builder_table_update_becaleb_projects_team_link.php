<?php namespace Becaleb\Projects\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBecalebProjectsTeamLink extends Migration
{
    public function up()
    {
        Schema::table('becaleb_projects_team_link', function($table)
        {
            $table->dropPrimary(['projects_id','team_id']);
            $table->renameColumn('team_id', 'teaam_id');
            $table->primary(['projects_id','teaam_id']);
        });
    }
    
    public function down()
    {
        Schema::table('becaleb_projects_team_link', function($table)
        {
            $table->dropPrimary(['projects_id','teaam_id']);
            $table->renameColumn('teaam_id', 'team_id');
            $table->primary(['projects_id','team_id']);
        });
    }
}
